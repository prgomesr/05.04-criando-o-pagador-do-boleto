CREATE TABLE IF NOT EXISTS `fatura_registrada` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `linha_digitavel` VARCHAR(255) NULL,
  `qrcode_url` VARCHAR(255) NULL,
  `qrcode_emv` VARCHAR(255) NULL,
  `criado_em` DATETIME NOT NULL,
  `atualizado_em` DATETIME NOT NULL,
  `fatura_id` BIGINT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_fatura_registrada_fatura1_idx` (`fatura_id` ASC),
  CONSTRAINT `fk_fatura_registrada_fatura1`
    FOREIGN KEY (`fatura_id`)
    REFERENCES `fatura` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB default charset = utf8;